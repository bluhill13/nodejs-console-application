const readline = require("readline");
const package = require('./package.json');
const os = require('os');
const http = require('http');
const hostname = '127.0.0.1';
const port = 3000;
const r1 = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

(function main(){
console.log("Choose an option:\n 1. Read package.json \n 2. Display OS info \n 3. Start HTTP server");
r1.question("Type a number: ", input => {
    switch(parseInt(input)){
        case 1:
            console.log(package);
            break;
        case 2:
            console.log(`Getting OS info...\n SYSTEM MEMORY: ${(os.totalmem() / 1024 / 1024 /1024).toFixed(2)} GB`);
            console.log(`FREE MEMORY: ${(os.freemem() / 1024 / 1024 /1024).toFixed(2)} GB`);
            console.log(`CPU CORES: ${os.cpus().length}`);
            console.log(`Arch: ${os.arch()}`);
            console.log(`PLATFORM: ${os.type()}`);
            console.log(`User: ${os.userInfo().username}`);
            break;
        case 3:
            const server = http.createServer((req, res) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/plain');
                res.end('Hello world');
                
            });
            server.listen(port, hostname, () => {
                console.log(`Server running at http://${hostname}:${port}/`);
            });
            break;
        default:
            console.log("An invalid option has been chosen!")
            break;
    }
    r1.close();
});


})();

